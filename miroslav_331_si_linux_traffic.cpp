#include <unistd.h>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <errno.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <iostream>
#include <arpa/inet.h> // inet_pton
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */


using std::cin;
using std::cout;
using std::endl;



int start(char* ip, unsigned short port) {

    in_addr ina;
	int ipt=inet_pton(AF_INET, ip, &ina);
        

 	sockaddr_in addr;
    addr.sin_addr.s_addr=ina.s_addr;
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);
 
    int sock = socket(AF_INET,SOCK_STREAM,IPPROTO_IP);
    
    
    int conn = connect(sock,(sockaddr*)&addr,sizeof(addr));
    if(conn!=0){
        cout<<"Greshka pri konekciqta"<<endl;
        cout<<conn<<endl;
        cout<<errno<<endl;
        return EXIT_FAILURE;
    }

    constexpr int BUFF_LEN=16;
    
    srand(time(0));
    
    char msg[BUFF_LEN];
    
    int rnum=0;
    
    while(1){
        for(int i=0;i<BUFF_LEN;i++){
                msg[i]=rnum=1+rand()%100;
        }
        
        int snd = send(sock,msg,strlen(msg)+1,0);
        if(snd!=-1){
            cout<<"Izprateno tova suobshtenie :"<<endl;
            cout<<msg<<endl;
            sleep(500);
            
        }else{
            cout<<"Error ne e izprateno suobshtenie"<<endl;
        }
        
    }

    return EXIT_FAILURE;


}

int main() {

    
    char ip[INET_ADDRSTRLEN];
    cout<<"Vuvedete dst ip address: "<<endl;
    cin>>ip;
    unsigned short port;
    cout<<"Vuvedete dst port: "<<endl;
    cin>>port;
    while(!cin.good()){
        cin.clear();
        cin.ignore(100,'\n');
        cout<<"Vuvedete cifri za port: ";
        cin>>port;
    }
    
    start(ip,port);
    
    
    
    
	

}











































#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

void start(char[]);
        
int main() {
    char ifname[IF_NAMESIZE]; //enp1s6;
    cout<<"Ime na mrejov interface ot koijto shte izprashtate paketi: "<<endl;
    cin>>ifname;
    ::start(ifname);
}

void start(char itfc_name[]) {
    
        int sock =0,sizebuff=0;
        
        sock = socket(AF_PACKET,SOCK_RAW,IPPROTO_RAW);
        
        // netw interface
        ifreq myif_name,myif_mac;
        
        strncpy(myif_name.ifr_ifrn.ifrn_name,itfc_name,IF_NAMESIZE-1);
        strncpy(myif_mac.ifr_ifrn.ifrn_name,itfc_name,IF_NAMESIZE-1);
        
        ioctl(3,SIOCGIFINDEX,&myif_name);
        ioctl(3,SIOCGIFHWADDR,&myif_mac);
        
        constexpr int N=31;
        char buff[N];
        
        // ethernet header
        ether_header *ethernetheader = (ether_header*)buff;
        
        //sockaddr_ll
        sockaddr_ll addrll;
        addrll.sll_family=AF_PACKET;
        // my interface index
        addrll.sll_ifindex=myif_name.ifr_ifru.ifru_ivalue;
        // addr lenght 6
        addrll.sll_halen=ETH_ALEN;
        
        for(size_t i=0;i<ETH_ALEN;i++){

	            ethernetheader->ether_dhost[i]=0xff;
            

        	    ethernetheader->ether_shost[i]=((uint8_t*)&myif_mac.ifr_ifru.ifru_hwaddr.sa_data)[i];
		    addrll.sll_addr[i]= 0xff;
         }
        
        //protocol
        ethernetheader->ether_type=htons(ETH_P_IP); // ETH_P_IP , ETH_P_ALL
        
        int cc=0;

        while(1){
            sizebuff = sizeof(ether_header);
         
            for(int i=0;i<16;i++){
                if(cc==0){
                    buff[sizebuff++]=0xaa;
                }
                else{
                    buff[sizebuff++]=0x55;
                }
            }

            
            if(sendto(sock,buff,sizebuff,0,(sockaddr*)&addrll,sizeof(addrll))!=0){
                    cc= cc==1?0:1;
                    sizebuff=sizeof(ether_header);                    
                    cout<<"Uspeshno izprateno suobshtenie"<<endl;
            }else{
                cout<<"Greshka pri izprashtane na suobshtenie"<<endl;
                break;
            }
            
        }
        
        
}









































